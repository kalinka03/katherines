<section class="katherines-cookies">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="border-box-katherines-cookies"
                     style="background: url(<?php the_field('katherines_image'); ?>);">
                    <div class="katherines-cookies_title">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block1/katherines.svg"
                             alt="">
                    </div>
                    <div class="katherines-cookies_information">
                        <p>It’s all about the cookies!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-story">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="our-story_title"
                     style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/catering/block2/backg.png);">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block2/title.png" alt="">
                </div>
                <div class="our-story_content">
                    <div class="our-story_item">
                        <div class="our-story_circle">
                            <p>01</p>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block2/1_hart.svg"
                                 alt="">
                        </div>
                        <div class="our-story_item_info">
                            <h3>1980</h3>
                            <p>Began with Love</p>
                        </div>
                    </div>
                    <div class="our-story_item item_line">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block2/line.png" alt="">
                    </div>
                    <div class="our-story_item">
                        <div class="our-story_circle purple">
                            <p>02</p>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block2/2_arrow.svg"
                                 alt="">
                        </div>
                        <div class="our-story_item_info">
                            <h3>80’, 90’, 00’</h3>
                            <p>A Story of Generations</p>
                        </div>
                    </div>
                    <div class="our-story_item item_line">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block2/line.png" alt="">
                    </div>
                    <div class="our-story_item">
                        <div class="our-story_circle">
                            <p>03</p>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block2/3_hous.svg"
                                 alt="">
                        </div>
                        <div class="our-story_item_info">
                            <h3>2013</h3>
                            <p>Home Business</p>
                        </div>
                    </div>

                    <div class="our-story_item">
                        <div class="our-story_circle purple">
                            <p>04</p>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block2/4_smile.svg"
                                 alt="">
                        </div>
                        <div class="our-story_item_info">
                            <h3>2016</h3>
                            <p>1000 Happy Customers</p>
                        </div>
                    </div>
                    <div class="our-story_item item_line">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block2/line.png" alt="">
                    </div>
                    <div class="our-story_item">
                        <div class="our-story_circle">
                            <p>05</p>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block2/5_shop.svg"
                                 alt="">
                        </div>
                        <div class="our-story_item_info">
                            <h3>2017</h3>
                            <p>First Branch</p>
                        </div>
                    </div>
                    <div class="our-story_item item_line">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block2/line.png" alt="">
                    </div>
                    <div class="our-story_item">
                        <div class="our-story_circle purple">
                            <p>06</p>
                            <img class="hands_item"
                                 src="<?php echo get_template_directory_uri(); ?>/img/catering/block2/6_hands.svg"
                                 alt="">
                        </div>
                        <div class="our-story_item_info">
                            <h3>2018</h3>
                            <p>Growing with Partners</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<section class="vision">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="vision_wrapper">
                    <div class="vision_left"
                         style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/catering/block3/backg.png);">
                        <div class="vision_title">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block3/title.png" alt="">
                        </div>
                        <div class="vision_text">
                            <p>
                                To be the go-to brand for a wide range of cookie varieties within the next 3 years in
                                Saudi
                                Arabia
                            </p>
                        </div>
                    </div>
                    <div class="vision_right">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block3/foto.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="why_katherines">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="why_katherines__wrapper">
                    <div class="why_katherines__title"
                         style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/catering/block4/backg.png);">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block4/title.png" alt="">
                    </div>
                    <div class="why_katherines__content">
                        <div class="why_katherines__item">
                            <div class="why_katherines__item__icon specialty">
                                <div class="why_katherines__item__pict">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block4/1_SPECIALTY.svg"
                                         alt="">
                                </div>
                            </div>
                            <div class="why_katherines__item__title">
                                <h3>SPECIALTY</h3>
                                <p>Cookies and only cookies</p>
                            </div>
                        </div>
                        <div class="why_katherines__item">
                            <div class="why_katherines__item__icon variety">
                                <div class="why_katherines__item__pict">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block4/2_VARIETY.svg"
                                         alt="">
                                </div>
                            </div>
                            <div class="why_katherines__item__title">
                                <h3>VARIETY</h3>
                                <p>Wide range, more than 30 types</p>
                            </div>
                        </div>
                        <div class="why_katherines__item">

                            <div class="why_katherines__item__icon quality">
                                <div class="why_katherines__item__pict">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block4/3_QUALITY.svg"
                                         alt="">
                                </div>
                            </div>
                            <div class="why_katherines__item__title">
                                <h3>QUALITY</h3>
                                <p>High quality ingredients almost free of preservatives</p>
                            </div>
                        </div>
                        <div class="why_katherines__item">

                            <div class="why_katherines__item__icon freshness">
                                <div class="why_katherines__item__pict">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block4/4_FRESHNESS.svg"
                                         alt="">
                                </div>
                            </div>
                            <div class="why_katherines__item__title">
                                <h3>FRESHNESS</h3>
                                <p>Built from scratch. Daily baked</p>
                            </div>
                        </div>
                        <div class="why_katherines__item">
                            <div class="why_katherines__item__icon innovation">
                                <div class="why_katherines__item__pict">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block4/5_INNOVATION.svg"
                                         alt="">
                                </div>
                            </div>
                            <div class="why_katherines__item__title">
                                <h3>INNOVATION</h3>
                                <p>Improving and upgrading our products and operations</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="strategy">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="strategy_wrapper">
                    <div class="strategy_container">
                        <div class="strategy-bg-container  right-content">
                            <div class="strategy-image"
                                 style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/catering/block5/foto.png);">
                            </div>
                        </div>
                        <div class="strategy__left">
                            <div class="strategy__left_container">
                                <div class="title">
                                    <h3>Strategy</h3>
                                </div>
                                <div class="information">
                                    <div class="information_item">
                                        <div class="col-left">
                                            <p>Retail</p>
                                        </div>
                                        <div class="col-right">
                                            <p>Provide our customers with a pleasant experience</p>
                                        </div>
                                    </div>
                                    <div class="information_item">
                                        <div class="col-left">
                                            <p>Catering</p>
                                        </div>
                                        <div class="col-right">
                                            <p>Supply businesses with our unique products</p>
                                        </div>
                                    </div>
                                    <div class="information_item">
                                        <div class="col-left">
                                            <p>Outlets</p>
                                        </div>
                                        <div class="col-right">
                                            <p>Partnering with big brands and working together to achieve mutual
                                                benefits</p>
                                        </div>
                                    </div>
                                    <div class="information_item">
                                        <div class="col-left">
                                            <p>White-label</p>
                                        </div>
                                        <div class="col-right">
                                            <p>Working with ambitious brands to create their own special recipes</p>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</section>



<section class="partners">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="partners_wrapper">
                    <div class="partners_container">
                        <div class="partners-bg-container">
                            <div class="partners-title">
                                <h3>Partners</h3>
                            </div>
                        </div>
                        <div class="partners_foto">
                        <div class="partners_slider owl-carousel">
                            <div class="partners_item">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block7/foto1.png"
                                     alt="">
                            </div>
                            <div class="partners_item">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block7/foto4.png"
                                     alt="">
                            </div>
                            <div class="partners_item">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block7/foto1.png"
                                     alt="">
                            </div>
                            <div class="partners_item">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block7/foto4.png"
                                     alt="">
                            </div>
                            <div class="partners_item">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/catering/block7/foto1.png"
                                     alt="">
                            </div>

                        </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>













