<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title(''); ?><?php if (wp_title('', false)) {
            echo ' :';
        } ?><?php bloginfo('name'); ?></title>

    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/Favicon.png" rel="shortcut icon">
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php bloginfo('description'); ?>">

    <?php wp_head(); ?>


</head>

<?php if (get_theme_mod('body_bg')) : ?>
    <?php $bodyBG = get_theme_mod('body_bg'); ?>
<?php endif; ?>


<body <?php body_class(); ?> style="background-image: url(<?php echo $bodyBG ?>);">

<!-- wrapper -->
<div class="main-wrapper">


    <!---------------------Mobile menu----------------------------->
    <div class="mobile-menu-container">
        <div class="mobile-menu-content-wrapper d-flex align-items-center justify-content-start">
            <div class="mobile-menu-wrapper">
                <button class="close-nav" type="button">
                    <span class="hamb-top"></span>
                    <span class="hamb-bottom"></span>
                </button>
                <div class="main-mobile-menu-container menu-block">
                    <div class="translation-mobile-menu">
                        fafaf
                    </div>
                    <!-- --><?php /*the_katherines_menu(); */ ?>
                    <?php main_menu_left(); ?>
                    <?php main_menu_right(); ?>
                </div>
            </div>

            <div class="menu-button-wrapper">
                <button class="mobile-button" type="button">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button>
            </div>
            <div class="mobile-logo-container">
                <?php if (function_exists('the_custom_logo')) {
                    the_custom_logo();
                }
                ?>
            </div>
        </div>
    </div>


    <!--------------------Desktop--------------------->
    <header class="header desktop-only">

        <div class="container">
            <div class="translation-nav">
                fafa
            </div>
        </div>


        <div class="main-menu-wrapper">
            <div class="container">
                <div class="menu-container" role="navigation">
                    <div class="left-menu-container">
                        <?php main_menu_left();
                        ?>
                    </div>
                    <div class="main-logo-container">
                        <?php if (function_exists('the_custom_logo')) {
                            the_custom_logo();
                        }
                        ?>
                    </div>
                    <div class="right-menu-container">
                        <?php main_menu_right(); ?>
                    </div>
                </div>

            </div>
        </div>


    </header>
    <!-- /header -->
