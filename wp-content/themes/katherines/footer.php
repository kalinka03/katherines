<!-- footer -->
<footer class="footer">

    <div class="footer-content-wrapper">
    <div class="container">
        <div class="footer-content">
            <div class="footer-logo-container">

                <?php if (function_exists('the_custom_logo')) {
                    the_custom_logo();
                }
                ?>

<!--                    <div class="social_net">-->
<!--                        <a target="_blank" href="https://www.facebook.com/Katherinesksa/">-->
<!--                            <img src="--><?php //echo get_template_directory_uri() ?><!--/assets/images/icon/facebook.svg" class="main" alt="">-->
<!--                            <img src="--><?php //echo get_template_directory_uri() ?><!--/assets/images/icon/facebook_active.svg" class="active" alt="">-->
<!--                        </a>-->
<!--                        <a target="_blank" href="https://www.instagram.com/katherines_sa/">-->
<!--                            <img src="--><?php //echo get_template_directory_uri() ?><!--/assets/images/icon/instagram.svg" class="main" alt="">-->
<!--                            <img src="--><?php //echo get_template_directory_uri() ?><!--/assets/images/icon/instagram_active.svg" class="active" alt="">-->
<!--                        </a>-->
<!--                        <a target="_blank" href="https://twitter.com/katherines_sa">-->
<!--                            <img src="--><?php //echo get_template_directory_uri() ?><!--/assets/images/icon/twitter.svg" class="main" alt="">-->
<!--                            <img src="--><?php //echo get_template_directory_uri() ?><!--/assets/images/icon/twitter_active.svg" class="active" alt="">-->
<!--                        </a>-->
<!--                        <a target="_blank" href="https://www.snapchat.com/add/katherines_sa">-->
<!--                            <img src="--><?php //echo get_template_directory_uri() ?><!--/assets/images/icon/snapchat.svg" class="main" alt="">-->
<!--                            <img src="--><?php //echo get_template_directory_uri() ?><!--/assets/images/icon/snapchat_active.svg" class="active" alt="">-->
<!--                        </a>-->
<!--                    </div>-->
            </div>
            <div class="row justify-content-sm-center footer_menu_content">
                
                <div class="col-sm-12 col-lg-11 col-md-11 col-xl-12">
                    <div class="footer-menu-container">
                        <?php footer_menu(); ?>
                    </div>
                    <div class="social_net">
                        <a target="_blank" href="https://www.facebook.com/Katherinesksa/">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/icon/facebook.svg" class="main" alt="">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/icon/facebook_active.svg" class="active" alt="">
                        </a>
                        <a target="_blank" href="https://www.instagram.com/katherines_sa/">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/icon/instagram.svg" class="main" alt="">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/icon/instagram_active.svg" class="active" alt="">
                        </a>
                        <a target="_blank" href="https://twitter.com/katherines_sa">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/icon/twitter.svg" class="main" alt="">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/icon/twitter_active.svg" class="active" alt="">
                        </a>
                        <a target="_blank" href="https://www.snapchat.com/add/katherines_sa">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/icon/snapchat.svg" class="main" alt="">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/icon/snapchat_active.svg" class="active" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
    <div class="footer-content-year">
        <div class="container">
<p><?php the_field('name_and_year_site'); ?></p>
        </div>
    </div>
</footer>
<!-- /footer -->

</div>
<!-- / main wrapper -->
<?php get_template_part('partials/delivery-popup'); ?>
<?php get_template_part('partials/popup-form'); ?>

<?php wp_footer(); ?>



</body>
</html>
