<section class="hero-banner-wrapper">

    <div class="container">

        <div class="row">

            <div class="col-12" data-aos="fade-down" data-aos-delay="100" data-aos-duration="1500" data-aos-once="true">

                <div class="wide-border-box">

                    <div class="wide-inner-border">

                        <div class="box-bg-container right-content">

                            <div class="box-image"

                                 style="background: url(<?php the_field('hero_banner_image'); ?>);"></div>

                        </div>

                    </div>

                    <div class="box-content-wrapper">

                        <div class="box-content-container" style="background-color: <?php the_field('banner_content_background'); ?>;">

                            <h1 class="title">

                                <?php the_field('banner_title'); ?>

                            </h1>

                            <?php if (have_rows('banner_content')): ?>



                            <div class="text-block">

                                <table>

                                <?php while (have_rows('banner_content')) : the_row(); ?>



                                    <tr>

                                        <td><?php the_sub_field('days'); ?></td>
                                        <?php if(!get_sub_field('closed')) : ?>
                                            <td><?php the_sub_field('from_time'); ?>-<?php the_sub_field('to_time'); ?></td>
                                        <?php else: ?>
                                            <td>Closed</td>
                                        <?php endif ?>

                                    </tr>



                                <?php endwhile; ?>

                                </table>

                            </div>

                            <?php endif; ?>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>