<div id="deliveri-popup" class="white-popup-block mfp-hide delivery-popup-container">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="popup-form-wrapper delivery-popup-wrapper">
                    <a href="javascript:void(0);"  class="close-popup">X</a>
                    <div class="border-box">
                        <div class="inner-border-box">
                            <div class="box-content popup-simple-text">
                                <?php the_field('delivery_popup_content'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>