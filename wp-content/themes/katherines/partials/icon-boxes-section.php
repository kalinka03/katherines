<section class="icon-boxes-section">

    <div class="container">

        <div class="row justify-content-between">

            <?php

            $firstIcon = get_field('first_box_icon');

            $secondIcon = get_field('second_box_icon');

            $thirdIcon = get_field('Third_box_icon');

            $firstButton = get_field('first_box_button');

            $secondButton = get_field('second_box_button');

            $thirdButton = get_field('third_box_button');

            ?>



            <!--Delivery box-->

            <div class="col-lg-4" data-aos="zoom-out" data-aos-delay="100" data-aos-once="true">

                <div class="border-box" style="background: url(<?php the_field('first_box_bg'); ?>);">

                    <div class="inner-border-box">

                        <div class="box-content">

                            <?php if ($firstButton) : ?>

                                <div class="button-box-wrapper">

                                    <div class="button-box">

                                        <a href="#deliveri-popup" class="delivery-popup"><?php echo $firstButton['title']; ?></a>

                                    </div>

                                </div>

                            <?php endif; ?>

                        </div>

                    </div>

                </div>

            </div>

            <!--End delivery box-->



            <!--Menu box-->

            <div class="col-lg-4" data-aos="zoom-out" data-aos-delay="200" data-aos-once="true">

                <div class="border-box" style="background: url(<?php the_field('second_box_bg'); ?>);">

                    <div class="inner-border-box">

                        <div class="box-content">

                            <?php if ($secondButton) : ?>

                                <div class="button-box-wrapper">

                                    <div class="button-box">

                                        <a href="<?php echo $secondButton; ?>" target="_blank"><?php the_field('second_box_button_text'); ?></a>

                                    </div>

                                </div>

                            <?php endif; ?>

                        </div>

                    </div>

                </div>

            </div>

            <!--End menu box-->





            <!--Discount box-->
            <?php if(get_field('show_third_box')): ?>
                <div class="col-lg-4" data-aos="zoom-out" data-aos-delay="300" data-aos-once="true">

                    <div class="border-box" style="background: url(<?php the_field('third_box_bg'); ?>);">

                        <div class="inner-border-box">

                            <div class="box-content">

                                <?php if ($thirdButton) : ?>

                                    <div class="button-box-wrapper">

                                        <div class="button-box">

                                            <a href="#form-popup" class="popup-with-form"><?php echo $thirdButton['title']; ?></a>

                                        </div>

                                    </div>

                                <?php endif; ?>

                            </div>

                        </div>

                    </div>

                </div>
            <?php endif; ?>

            <!--End discount box-->



        </div>





    </div>

</section>



