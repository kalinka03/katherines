<section class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="wide-border-box">
                    <div class="wide-inner-border map-box">
                        <div class="box-bg-container left-content ">
                            <div class="map-container">
                                <?php echo do_shortcode('[google_map_easy id="1"]')?>
                            </div>

                        </div>
                    </div>
                    <div class="box-content-wrapper left-content-box welcome-box">
                        <div class="box-content-container" style="background-color: <?php the_field('contact_content_background'); ?>;">
                            <h1 class="title">
                                <?php the_field('section_title'); ?>
                            </h1>

                                <div class="text-block contact-info-block">
                               <?php the_field('contact_content'); ?>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>