(function ($) {


	$(document).ready(function() {
        $(".partners_slider").owlCarousel({
            slidesToShow: 1,
            slidesToScroll: 1,
            loop: true,
            autoHeight: true,
            nav: true,
            item: 1,
            dots: false,
            navText: ["", ""],
            responsive: {
                0: {
                    items: 1
                },
                481: {
                    items: 2
                },
                765: {
                    items: 3
                },
                1023: {
                    items: 4
                },
            }
		})




		//Mobile menu

		$(".menu-button-wrapper button").click(function () {
			$('.main-body-wrapper').addClass('main-wrapper-push');
			$('.mobile-menu-wrapper').addClass('visible');
			$('.menu-button-wrapper').addClass('hidden');
			$('.close-nav').addClass('visible');
			$('.mobile-menu-wrapper .navbar-collapse').addClass('in');
		});

		$(".close-nav").click(function () {
			$('.main-body-wrapper').removeClass('main-wrapper-push');
			$('.mobile-menu-wrapper').removeClass('visible');
			$('.menu-button-wrapper').removeClass('hidden');
			$('.close-nav').removeClass('visible');
			// $('.mobile-menu-wrapper .navbar-collapse').removeClass('in');
		});


		addPopupClass();
		$('.close-popup').on( "click", function() {
			$.magnificPopup.close();
		});

		$('.popup-with-form').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '#name',

			// When elemened is focused, some mobile browsers in some cases zoom in
			// It looks not nice, so we disable it:
			callbacks: {
				beforeOpen: function() {
					if($(window).width() < 700) {
						this.st.focus = false;
					} else {
						this.st.focus = '#name';
					}
				}
			}
		});

		$('.delivery-popup').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '#name',

			// When elemened is focused, some mobile browsers in some cases zoom in
			// It looks not nice, so we disable it:
			callbacks: {
				beforeOpen: function() {
					if($(window).width() < 700) {
						this.st.focus = false;
					} else {
						this.st.focus = '#name';
					}
				}
			}
		});



		var url = new URL(window.location.href);
		if(url.searchParams.get('success')){//http://katerin/?success=1&phone={data key="PHONE"}&mail={data key="EMAIL"}
			$.magnificPopup.open({
			  items: {
			    src: '#form-popup_thanks'
			  },
			  type: 'inline'
			});
		}

		var unique_submit = false;
		$('#mc4wp-form-1').submit(function() {
			if(!unique_submit){
				var phone = $(this).find('[name=PHONE]').val();
				var email = $(this).find('[name=EMAIL]').val();
				$.get('http://katerin/wp-json/art-sites/v2/unique?phone='+phone+'&mail='+email).success(function(result) {
					var data = JSON.parse(result);
					debugger;
					if(data['unique']){
						unique_submit = true;
						$('#mc4wp-form-1').submit();
						$('.form-description__discount').remove();
					}else{
						$('.form-description__discount').remove();
						$('<p>', {class: 'form-description__discount'}).text('The discount was used for this user').appendTo('.form-description')
					}
					
				})
				return false;
			}else{
				unique_submit = false;
				return true;
			}
			
		})



	});

//end document ready

	function addPopupClass(){
		$('.popup-menu-item a').addClass('popup-with-form');
		$('.delivery-popup-item a').addClass('delivery-popup');

	}

    function partnersSlider(){
        $('.popup-menu-item a').addClass('popup-with-form');
        $('.delivery-popup-item a').addClass('delivery-popup');

    }


})(jQuery);





