<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'katherines_wp');
// define('WP_HOME', 'http://katerin/');
// define('WP_SITEURL', 'http://katerin/');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', '127.0.0.1');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'fllzwnazvt1ljw2t43tcs29wyo5pihopwgnyqdqskl90bo1bscxnqadorryfd0je');
define('SECURE_AUTH_KEY',  'beuczkr0bs9lbdk9mrxzt2nszzvuhi0b0qnzdnwinxo8gohjjfsn1kqoukgr9zct');
define('LOGGED_IN_KEY',    'jlnlg8fwzq1jtfyhi8ljomar7kiem5ke76hmabmnaow6po8plcnrzefw2wqvxfld');
define('NONCE_KEY',        'kwlykebuyfhjrq242pekfgbnze6ajnmpm1foq0iqbtgib4v5k4unggvyhtnsoxuq');
define('AUTH_SALT',        'o2bysjkdfnga5eyxanhphc8jao8tw02lth86hiqqv2uitevyhymbadcczgqnuqga');
define('SECURE_AUTH_SALT', 'zdwfnsmqhtu7zzjhiujbkudgk2vqmu7hwfywyj1wur9zck233rafxw67adyefmk7');
define('LOGGED_IN_SALT',   'epfk64hlxmismitw1zpkrze5unccuyasfd2c7up8wlhqv2syzutyejzhfvshdfya');
define('NONCE_SALT',       'cfjmhri9lboztvpy2dc0h0xxrfwytczp6q0wsdhfgbhgzzvo0pppudjw7pcfyu5e');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpui_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
